Python ListenBrainz client module
**********************************

python-listenbrainz is an HTTP client to consume ListenBrainz HTTP API.


----

:Documentation: https://kaliko.gitlab.io/python-listenbrainz
:HTTP-API:      https://listenbrainz.readthedocs.io/en/latest/dev/api.html
:Code:          https://gitlab.com/kaliko/python-listenbrainz
:Dependencies:  `requests`_ http library.
:Compatibility: Python 3.4+
:Licence:       GNU GPLv3

.. WARNING::
   **Module still in developement phase**

   Version 0.x.x are development release.

   Module API **will** break anyway since Listensbrainz is in beta stage.

.. _requests: http://docs.python-requests.org/en/master/

----
