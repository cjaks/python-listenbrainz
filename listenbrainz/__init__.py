# coding: utf-8
# Copyright (C) 2018  Kaliko Jack <kaliko@azylum.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""python-listenbrainz, a plain python module to build listenbrainz clients.
"""

#: Optionnal "additional_info" elements allowed for listens
ADDITIONAL_INFO_ITEMS = {
    'artist_mbids',
    'artist_msid',
    'release_group_mbid',
    'release_mbid',
    'release_msid',
    'recording_mbid',
    'track_mbid',
    'work_mbids',
    'tracknumber',
    'isrc',
    'spotify_id',
    'tags'}

#: SERVER ROOT URL
ROOT_URL = 'https://api.listenbrainz.org'
API_VERSION = '1'
TIMEOUT = 5

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
