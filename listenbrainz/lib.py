# coding: utf-8
# Copyright (C) 2018  Kaliko Jack <kaliko@azylum.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Utility classes and functions
"""

import logging
import re

from functools import wraps
from time import time

from .exceptions import ListenBrainzException
from . import ADDITIONAL_INFO_ITEMS

UUID_RE = r'^[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}$'
_EX = {
          "listened_at": 1443521965,
          "track_metadata": {
            "artist_name": "Rick Astley",
            "track_name": "Never Gonna Give You Up",
            "release_name": "Whenever you need somebody",
            "additional_info": {
              "release_mbid": "bf9e91ea-8029-4a04-a26a-224e00a83266",
              "artist_mbids": [
                "db92a151-1ac2-438b-bc43-b82e149ddd50"
              ],
              "recording_mbid": "98255a8c-017a-4bc7-8dd6-1fa36124572b",
              "tags": [ "you", "just", "got", "rick rolled!"]
            },
          }
        }

def is_uuid4(uuid):
    """Controls MusicBrainz UUID4 format

    :param str uuid: String representing the UUID
    :returns: boolean
    :rtype: bool
    """
    regexp = re.compile(UUID_RE, re.IGNORECASE)
    if regexp.match(uuid):
        return True
    return False

def auth_needed(func):
    """Decorator to check token presence"""
    @wraps(func)  # cf. https://github.com/sphinx-doc/sphinx/issues/1881
    def wrapper(*args, **kwargs):
        cli_inst = args[0]
        if not cli_inst.user.get('token'):
            raise ListenBrainzException('Missing token, Authorization needed for this call!')
        return func(*args, **kwargs)
    return wrapper

class Listen:
    """
    Object representing a listen. In plain word, a track description with a timestamp.

    :param str title: track name
    :param str artist: artist name
    :keyword int ts: UTC timestamp (set to currentime if left to `None`)

    - **mandatory parameters**

      Instanciate with at least track **name** and **artist** name.

    - **optional parameters**

      - Current timestamp is used if not given in **ts**.

      - Additional info such as MusicBrainz IDs are extracted from keywords
        parameters. Only keywords from :py:obj:`listenbrainz.ADDITIONAL_INFO_ITEMS` tuple are used.

    .. todo::
        Use is_uuid4 to controls MusicBrainzID
    """

    def __init__(self, title, artist, listened_at=None, **kwargs):
        self._log = logging.getLogger(__name__)
        self._metadata = {'artist_name': artist, 'track_name': title}
        self._listened_at = listened_at if listened_at else int(time())
        self._metadata.update({'additional_info': {}})
        if 'release_name' in kwargs:
            self._metadata.update({'release_name': kwargs.pop('release_name')})
        self._update_additional(kwargs)
        self._listen = {'listened_at': self._listened_at,
                        'track_metadata': self._metadata}

    def _update_additional(self, kwargs):
        """Auto discovery of additional info from kwargs
        """
        for item in ADDITIONAL_INFO_ITEMS & set(kwargs.keys()):
            add_info = self._metadata.get('additional_info')
            self._log.debug('Got additional_info: %s: %s', item, kwargs.get(item))
            add_info.update({item: kwargs.pop(item)})

    def __repr__(self):
        args = 'title="{0.title}", artist="{0.artist}", listened_at="{0.listened_at}'
        return "%s(%s)" % (self.__class__.__name__, args.format(self))

    def payload(self, with_ts=True):
        """
        Returns the payload as a python object

        :params bool with_ts: Add timestamp in payload
        :returns: a listen as `documented <https://listenbrainz.readthedocs.io/en/latest/dev/json.html#payload-json-details>`_.
        :rtype: dict
        """
        listen = self._listen.copy()
        if not with_ts:
            listen.pop('listened_at')
            return listen
        return listen

    @property
    def listened_at(self):
        """
        :returns: Timestamp
        :rtype: int
        """
        return self._listen.get('listened_at')

    @property
    def artist(self):
        """
        :returns: Artist name
        :rtype: str
        """
        return self.track_metadata.get('artist_name')

    @property
    def title(self):
        """
        :returns: Track name
        :rtype: str
        """
        return self.track_metadata.get('track_name')

    @property
    def track_metadata(self):
        """
        :returns: Track Metadata, MusicBrainzID etc...
        :rtype: dict
        """
        return self._listen.get('track_metadata')

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
