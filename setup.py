#! /usr/bin/env python3
# coding: utf-8
"""python-listenbrainz packaging setup
"""

from setuptools import setup, find_packages

from listenbrainz.info import __version__ as VERSION, \
                              __doc__ as LDESCRIPTION, \
                              __url__ as URL, \
                              __author__ as AUTHOR, \
                              __email__ as AUTHOR_EMAIL

CLASSIFIERS = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    "Natural Language :: English",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
    "Topic :: Software Development :: Libraries :: Python Modules",
]


setup(
    name='python-listenbrainz',
    version=VERSION,
    description='A plain python module to build listenbrainz clients.',
    long_description=LDESCRIPTION,
    long_description_content_type='text/x-rst',
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    url='https://kaliko.gitlab.io/python-listenbrainz',
    download_url='https://pypi.python.org/simple/python-listenbrainz/' \
                 'python-listenbrainz-{}.tar.xz'.format(VERSION),
    packages=find_packages(exclude=['doc', 'tests*']),
    classifiers=CLASSIFIERS,
    license='GPLv3',
    keywords=["musicbrainz"],
    platforms=["Independant"],
    install_requires=['requests>= 2.4.0'],
    test_suite="tests",
    project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/kaliko/python-listenbrainz/issues',
        'Funding': 'https://liberapay.com/kaliko/',
        'Source': URL,
        },
)


# vim: set expandtab shiftwidth=4 softtabstop=4 textwidth=79:
