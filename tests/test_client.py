#!/usr/bin/python3
# coding: utf-8

import unittest

from copy import deepcopy
from json import loads as json_loads
from unittest.mock import patch, MagicMock, Mock
from time import time

import requests

from listenbrainz.core import Client, Listen, TIMEOUT, Request
from listenbrainz.core import ListenBrainzException, ListenBrainzHttpException
from listenbrainz.core import ROOT_URL, API_VERSION

JSON_DATA="""{
  "payload": {
    "count": 1,
    "listens": [
      {
        "listened_at": 1521484061,
        "recording_msid": "4e24f1d7-a266-4a29-b2c4-37283aa476ec",
        "track_metadata": {
          "additional_info": {
            "artist_mbids": [],
            "artist_msid": "5c05fa9c-d2ad-4f44-88e1-4c0624a5a347",
            "artist_name": "Cypress Hill",
            "isrc": null,
            "recording_mbid": "021c3963-81ec-4203-ab83-fd417fa0c4f9",
            "release_group_mbid": null,
            "release_mbid": null,
            "release_msid": "3b4048be-28bb-4935-837c-1404c48610ea",
            "release_name": "Los grandes exitos en espanol",
            "source": "P",
            "spotify_id": null,
            "tags": [],
            "track_length": "204",
            "track_mbid": null,
            "track_name": "Loco en el coco",
            "track_number": "2",
            "tracknumber": null,
            "work_mbids": []
          },
          "artist_name": "Cypress Hill",
          "track_name": "Loco en el coco"
        }
      }
    ],
    "user_id": "kaliko"
  }
}"""


class TestClientObject(unittest.TestCase):

    def setUp(self):
        self.cli = Client('listenb-user', 'listenb-token')

    def mock_resp(self, status=200, content="CONTENT",
                  json_data=JSON_DATA, raise_for_status=None,
                  headers=None):
        #  https://gist.github.com/evansde77/45467f5a7af84d2a2d34f3fcb357449c
        mock_resp = Mock()
        # mock raise_for_status call w/optional error
        mock_resp.raise_for_status = Mock()
        if raise_for_status:
            mock_resp.raise_for_status.side_effect = raise_for_status
        # set status code and content
        mock_resp.status_code = status
        mock_resp.content = content
        # add json data if provided
        if json_data:
            mock_resp.json = Mock(
                return_value=json_loads(json_data)
            )
        mock_resp.headers = {k:str(v) for k, v in Client.RateLimit.items()}
        mock_resp.headers.update({'content-type': 'application/json'})
        if headers:
            mock_resp.headers.update(headers)
        return mock_resp

    @patch('listenbrainz.core.Session.send',)
    def test_authorization(self, sess_mock):
        sess_mock.return_value = self.mock_resp()
        self.cli.submit_single(Listen('test','test'))
        args, kwargs = sess_mock.call_args
        prep_req = args[0]
        self.assertIn('Authorization', prep_req.headers)

    @patch('listenbrainz.core.Session.send',)
    def test_rate_limit(self, sess_mock):
        ans_headers = {'X-RateLimit-Limit': '42', 'X-RateLimit-Remaining': '0',
                'X-RateLimit-Reset': str(int(time())+5), 'X-RateLimit-Reset-In': '1'}
        sess_mock.return_value = self.mock_resp(headers=ans_headers)
        _ = self.cli.listens()
        for k, v in ans_headers.items():
            self.assertIn(k, Client.RateLimit)
            self.assertEqual(Client.RateLimit.get(k), int(ans_headers.get(k)))
        with patch('listenbrainz.core.sleep') as mock_sleep:
            sess_mock.return_value = self.mock_resp(headers=ans_headers)
            _ = self.cli.listens()
            sleep_time = int(ans_headers['X-RateLimit-Reset-In'])
            mock_sleep.assert_called_once_with(sleep_time)

    @patch('listenbrainz.core.Session.send',)
    def test_rate_limit_sleep(self, sess_mock):
        ans_headers = {'X-RateLimit-Limit': '42', 'X-RateLimit-Remaining': '0',
                'X-RateLimit-Reset': str(int(time())+5), 'X-RateLimit-Reset-In': '1'}
        sess_mock.return_value = self.mock_resp(headers=ans_headers)
        with patch('listenbrainz.core.sleep') as mock_sleep:
            _ = self.cli.listens()
            sleep_time = int(ans_headers['X-RateLimit-Reset-In'])
            mock_sleep.assert_called_once_with(sleep_time)

    @patch('listenbrainz.core.Session.send',)
    def test_api_call_listens(self, sess_mock):
        """Testing api call **listens**"""
        sess_mock.return_value = self.mock_resp()
        self.cli.listens()
        call_args = sess_mock.call_args
        self.assertTrue(len(call_args) == 2)
        # Check send is called with correct timeout value
        timeout = call_args[1]
        args, kwargs = sess_mock.call_args
        self.assertTrue(kwargs['timeout'] == TIMEOUT)
        # check request…
        # print("call args %r / kwargs %r" % (args, kwargs))
        prep_req = args[0]
        self.assertIsInstance(prep_req, requests.models.PreparedRequest)
        ##              method is "get" for listens call
        self.assertTrue(prep_req.method == "GET")
        ##              does not use Authorization token. *listens* are public,
        ##              and auth defaults to False in listenbrainz.core.Client._request)
        self.assertNotIn('Authorization', prep_req.headers)
        ##              url is well formated
        self.assertEqual(prep_req.url, '{}/{}'.format(self.cli.url, 'user/listenb-user/listens'))

    @patch('listenbrainz.core.Session.send',)
    def test_api_call_submit_single(self, sess_mock):
        """Testing api call **sumbit-listens / single** (Usefull testing‽)"""
        sess_mock.return_value = self.mock_resp()
        listen = Listen('Autechre', 'feed1')
        self.cli.submit_single(listen)
        call_args = sess_mock.call_args
        self.assertTrue(len(call_args) == 2)
        # Check send is called with correct timeout value
        timeout = call_args[1]
        args, kwargs = sess_mock.call_args
        self.assertTrue(kwargs['timeout'] == TIMEOUT)
        # check request…
        # print("call args %r / kwargs %r" % (args, kwargs))
        prep_req = args[0]
        ##              method is "get" for listens call
        self.assertTrue(prep_req.method == "POST")
        ##              use Authorization token.
        self.assertIn('Authorization', prep_req.headers)
        ##              url is well formated
        self.assertEqual(prep_req.url, '{}/{}'.format(self.cli.url, 'submit-listens'))
        with self.assertRaises(ListenBrainzException):
            self.cli.submit_single(object())

    @patch('listenbrainz.core.Session.send',)
    def test_api_call_playing_now(self, sess_mock):
        """Testing api call **submit-listens / playing-now** (Usefull testing‽)"""
        sess_mock.return_value = self.mock_resp()
        # playing now should not contain timestamp
        listen = Listen('Parabolla', 'Tool')
        self.cli.submit_playing_now(listen)
        args, kwargs = sess_mock.call_args
        # Could mock listenbrainz.core.Client._request instead for this assert
        self.assertNotIn('listened_at', args[0].body.decode('utf8'))
        with self.assertRaises(ListenBrainzException):
            self.cli.submit_playing_now(object())

    @patch('listenbrainz.core.Session.send',)
    def test_api_call_submit_import(self, sess_mock):
        """Testing api call **submit-listens / import** (Usefull testing‽)"""
        sess_mock.return_value = self.mock_resp()
        listen = Listen('Parabolla', 'Tool')
        listens = [listen, object()]
        with self.assertRaises(ListenBrainzException):
            self.cli.submit_import(listens)

    @patch('listenbrainz.core.Session.send',)
    def test_execption_raised(self, sess_mock):
        sess_mock.return_value = self.mock_resp(status=500)
        with self.assertRaises(ListenBrainzHttpException):
            self.cli.listens()
        sess_mock.return_value = self.mock_resp()
        sess_mock.return_value.headers['content-type'] = 'foo'
        with self.assertRaises(ListenBrainzHttpException):
            self.cli.listens()

if __name__ == '__main__':
    unittest.main()

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
