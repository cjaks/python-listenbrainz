#!/usr/bin/env python3
# coding: utf-8

import unittest
import time

from listenbrainz.lib import Listen, is_uuid4

VALID = '110e8100-e29b-41d1-a716-116655250000'

class TestListenObject(unittest.TestCase):

    def testTimeStamps(self):
        tmstp = int(time.time())
        lst = Listen('Bosses Hang Pt. II ', 'Godspeed You! Black Emperor')
        payload = lst.payload()
        self.assertAlmostEqual(payload.get('listened_at'), tmstp, delta=10)

    def test_uuid_integrity(self):
        wrong = VALID +'a'
        self.assertFalse(is_uuid4(wrong))
        #  test UUID4 format validation
        self.assertFalse(is_uuid4(VALID.replace('4', '3')))
        self.assertFalse(is_uuid4(VALID.replace('a', 'z')))

def main():
    pass

# Script starts here
if __name__ == '__main__':
    main()

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
