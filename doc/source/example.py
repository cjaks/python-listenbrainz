from config import username, token
from listenbrainz.core import Client, Listen
cli = Client(username, token)
# Get Listen
lst = cli.listens()
# Push a listen
trk_info = {
    'artist': 'Mantar',
    'name': 'The Huntsmen',
    'artist_mbids': ['371c472e-2fc8-402b-9154-61776163d470'],
    'track_mbid': 'b711247f-7343-42cd-a328-01dd9a7c01da'
}
# timestamps is generated with Listen instance
lst = Listen(**trk_info)
cli.submit_single(lst)
listens = cli.listens(count=1)
print(listens)
