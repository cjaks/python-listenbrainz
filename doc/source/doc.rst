Documentation
==============

Examples
---------

.. literalinclude:: example.py
   :language: python3

.. literalinclude:: example.json
   :language: javascript

Main Objects
-------------

Listenbrainz http client
^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: listenbrainz.core.Client
    :members:
    :noindex:

Listen object
^^^^^^^^^^^^^^

.. autoclass:: listenbrainz.lib.Listen
    :members:
    :noindex:
    :undoc-members:


.. vim: spell spelllang=en
